const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const question = (question) => {
  return new Promise((resolve, reject) => {
    rl.question(question, (answer) => {
      resolve(answer);
    });
  });
};

const tambah = async () => {
  const angka1 = await question("masukkan angka pertama : ");
  const angka2 = await question("masukkan angka kedua : ");
  const result = parseInt(angka1) + parseInt(angka2);
  console.log(`Hasil Operasi ${angka1} + ${angka2} = ${result}`);
  rl.close();
};
const kurang = async () => {
  const angka1 = await question("masukkan angka pertama : ");
  const angka2 = await question("masukkan angka kedua : ");
  const result = parseInt(angka1) - parseInt(angka2);
  console.log(`Hasil Operasi ${angka1} - ${angka2} = ${result}`);
  rl.close();
};
const kali = async () => {
  const angka1 = await question("masukkan angka pertama : ");
  const angka2 = await question("masukkan angka kedua : ");
  const result = parseInt(angka1) * parseInt(angka2);
  console.log(`Hasil Operasi ${angka1} * ${angka2} = ${result}`);
  rl.close();
};
const bagi = async () => {
  const angka1 = await question("masukkan angka pertama : ");
  const angka2 = await question("masukkan angka kedua : ");
  const result = parseInt(angka1) / parseInt(angka2);
  console.log(`Hasil Operasi ${angka1} / ${angka2} = ${result}`);
  rl.close();
};
const kuadrat = async () => {
  const angka1 = await question("masukkan angka pertama : ");
  const angka2 = await question("masukkan angka kedua : ");
  const result = parseInt(angka1) ** parseInt(angka2);
  console.log(`Hasil Operasi ${angka1} ^ ${angka2} = ${result}`);
  rl.close();
};
const luasPersegi = async () => {
  const s = await question("masukkan panjang sisi : ");
  const result = parseInt(s) * parseInt(s);
  console.log(`Hasil Operasi Luas Persegi = ${s} * ${s} = ${result}`);
  rl.close();
};
const volumeKubus = async () => {
  const rusuk = await question("masukkan panjang rusuk : ");
  const result = parseInt(rusuk) * parseInt(rusuk) * parseInt(rusuk);
  console.log(
    `Hasil Operasi Volume Kubus : ${rusuk} * ${rusuk} * ${rusuk}  = ${result}`
  );
  rl.close();
};
const volumeTabung = async () => {
  const phi = 3.14;
  const r = await question("masukkan jari - jari : ");
  const t = await question("masukkan tinggi : ");

  const result = phi * parseInt(r) * parseInt(r) * parseInt(t);
  console.log(
    `Hasil Operasi Volume Tabung :  ${phi} * ${r} * ${r} * ${t} = ${result}`
  );
  rl.close();
};

const main = async () => {
  console.log(`list operasi :
  1. Tambah (+)
  2. Kurang (-)
  3. Kali (*)
  4. Bagi (/)
  5. akar kuadrat (^)
  6. Luas Persegi
  7. Volume Kubus
  8. Volume Tabung
  `);
  const operasi = await question("operasi : ");

  switch (operasi) {
    case "+":
      tambah();
      break;
    case "-":
      kurang();
      break;
    case "*":
      kali();
      break;
    case "/":
      bagi();
      break;
    case "^":
      kuadrat();
      break;
    case "6":
      luasPersegi();
      break;
    case "7":
      volumeKubus();
      break;
    case "8":
      volumeTabung();
      break;
    default:
      rl.close();
      break;
  }
};

main();
